#include <stdio.h>
#include "dice.h"

int main(){
	int n;
	initializeSeed();
	printf("Number of faces: ");
	scanf("%d", &n);
	printf("Let's roll the dice: %d\n", rollDice(n));
	return 0;
}
//dado virtual aleatorio
